
const entryEls = document.querySelectorAll('.entry')
const totalEl = document.getElementById('total')

let data = [0, 0, 0, 0]
const update = () => {
  data = data.map(v => 20 + (Math.random() * 15 | 0))
  setTimeout(update, 2000)
}

const render = () => {
  const sum = data.reduce((sum, v) => sum + v)
  const percentages = data.map(v => v / sum)
  const max = Math.max(...percentages)
  const values = percentages.map(v => v / max)
  values.forEach((v, i) => {
    entryEls[i].querySelector('.progress').value = v
  })
  totalEl.textContent = sum
  setTimeout(render, 300)
}

update()
setTimeout(render, 300)
